// @flow

import React from 'react';
import {createBottomTabNavigator, createAppContainer} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome';

import Home from '../stackNavigators/Home';
import {tabBarIconsSize} from '../../core/constants/styleConstants/Sizes';
import {
    tabIconActiveColor,
    tabIconInactiveColor,
    tabLabelActiveColor,
    tabLabelInactiveColor,
} from '../../core/constants/styleConstants/Colors';
import {homeIcon} from '../../core/constants/iconsConstants/IconsConstants';
import styles from './styles';

export default createAppContainer(createBottomTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                tabBarIcon: ({focused}) => (
                    focused
                        ? (
                            <Icon
                                name={homeIcon}
                                size={tabBarIconsSize}
                                color={tabIconActiveColor}
                            />
                        )
                        : (
                            <Icon
                                name={homeIcon}
                                size={tabBarIconsSize}
                                color={tabIconInactiveColor}
                            />
                        )
                ),
            },
        },
    },
    {
        tabBarOptions: {
            style: styles.tabStyle,
            labelStyle: styles.labelStyle,
            showLabel: true,
            activeTintColor: tabLabelActiveColor,
            inactiveTintColor: tabLabelInactiveColor,
        },
    },
));
