import { StyleSheet } from 'react-native';
import { tabBorderColor, tabBackgroundColor } from '../../core/constants/styleConstants/Colors';
import { montserratMedium } from '../../core/constants/fontsContstants/Fonts';

const styles = StyleSheet.create({
    tabStyle: {
        height: 60,
        padding: 5,
        borderTopWidth: 1,
        borderTopColor: tabBorderColor,
        backgroundColor: tabBackgroundColor,
    },
    labelStyle: {
        fontFamily: montserratMedium,
    },
});

export default styles;
