import { createStackNavigator } from 'react-navigation';

import HomeContainer from '../../home/containers/homeContainer/HomeContainer';
import CoinDetailsContainer from '../../home/containers/coinDetailsContainer/CoinDetailsContainer';

const Home = createStackNavigator({
    HomeContainer,
    CoinDetailsContainer,
  },
  {
    headerMode: 'none',
  },
);

export default Home;
