import { combineReducers } from 'redux';

import CoinsReducer from '../../home/reducers/CoinsReducer';
import CoinDetailsReducer from '../../home/reducers/CoinDetailsReducer';

export default combineReducers({
    coins: CoinsReducer,
    coinDetails: CoinDetailsReducer,
});
