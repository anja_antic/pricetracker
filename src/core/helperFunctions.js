import {Platform} from 'react-native';

/** Convert array of strings to array of numbers (use: converting array of prices for graph) * */
const convertArrayOfStringsToArrayOfNumbers = givenArray => givenArray.map(item => parseFloat(item));

/** Convert string to number * */
const convertStringToNumber = givenString => parseFloat(givenString);
/** Format current price (result: '1000.00000' -> '$1,000.00000) * */
const formatCurrentPrice = (arrayOfPrices) => {
    // pick the last price from array (current price) and convert it to number
    let currentPrice = convertStringToNumber(arrayOfPrices[arrayOfPrices.length - 1]);
    // format price
    if (Platform.OS === 'ios') {
        currentPrice = (currentPrice).toLocaleString('en-US', {
            style: 'currency',
            currency: 'USD',
            maximumFractionDigits: 5,
        });
        return currentPrice;
    } else {
        const dolarSign = '$';
        currentPrice = (currentPrice).toFixed(5).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        const finalCurrentPrice = dolarSign.concat(currentPrice);
        return finalCurrentPrice;
    }
};

/** Format date that has one number, with adding '0' in front (use: for start route parameter) * */
const formatDate = (dateNumber) => {
    let finalDateString = dateNumber.toString();
    // if date number has one digit (example '5')- add '0' at the beggining (returns '05')
    for (let i = 0; i < finalDateString.length; i++) {
        if (finalDateString.length === 1) {
            const zero = '0';
            finalDateString = zero.concat(finalDateString);
        }
    }
    return finalDateString;
};

export {convertArrayOfStringsToArrayOfNumbers, convertStringToNumber, formatCurrentPrice, formatDate};
