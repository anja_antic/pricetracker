// @flow

export type Coin = {
    currency: string,
    prices: Array,
    timestamps: Array,
};

const initialCoinState = {
    currency: '',
    prices: [0],
    timestamps: [],
};

export default initialCoinState;
