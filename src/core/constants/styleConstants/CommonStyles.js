import { ifIphoneX } from 'react-native-iphone-x-helper';
import { screenHeight } from './Sizes';

/** Headers * */
const commonHeaderStyle = {
  height: screenHeight / 8,
  ...ifIphoneX({
    height: screenHeight / 10,
  }),
  paddingBottom: 15,
};

export { commonHeaderStyle };
