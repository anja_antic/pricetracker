/** Fonts * */
const regularFontColor = '#000000';
const invertFontColor = '#FFFFFF';
const lightFontColor = '#A0A0A0';

/** Gradient * */
const gradientStartColor = '#5B5BF7';
const gradientMiddleColor = '#5327D1';
const gradientEndColor = '#761FB2';

/** Borders and Lines * */
const lightBorderColor = '#ECECEC';
const headerBorderColor = '#D1D1D1';
const graphGridColor = '#E6E6E6';
const tabBorderColor = '#D1D1D1';

/** Tab * */
const tabBackgroundColor = '#F7F7F7';
const tabIconActiveColor = '#000000';
const tabIconInactiveColor = '#A0A0A0';
const tabLabelActiveColor = '#000000';
const tabLabelInactiveColor = '#A0A0A0';

/** Icons * */
const iconRegularColor = '#000000';

/** Charts * */
const lineChartColor = '#761FB2';


export {
    regularFontColor,
    invertFontColor,
    lightFontColor,
    gradientStartColor,
    gradientMiddleColor,
    gradientEndColor,
    lightBorderColor,
    headerBorderColor,
    graphGridColor,
    tabBorderColor,
    tabBackgroundColor,
    tabIconActiveColor,
    tabIconInactiveColor,
    tabLabelActiveColor,
    tabLabelInactiveColor,
    iconRegularColor,
    lineChartColor,
};
