import { Dimensions } from 'react-native';

/** Screen Sizes * */
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;

/** Font Sizes * */
const heading1 = 27;
const heading2 = 20;
const heading3 = 18;
const heading4 = 14;
const heading5 = 12;
const heading6 = 10;

/** Icons * */
const coinSymbolSize = 20;
const tabBarIconsSize = 30;
const headerIconsSize = 20;

export {
  screenWidth,
  screenHeight,
  heading1,
  heading2,
  heading3,
  heading4,
  heading5,
  heading6,
  coinSymbolSize,
  tabBarIconsSize,
  headerIconsSize,
};
