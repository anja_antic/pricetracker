const homeIcon = 'home';
const arrowLeft = 'arrow-left';

/** Coins * */
const bitcoinIcon = 'https://cdn2.iconfinder.com/data/icons/cryptocurrency-5/100/cryptocurrency_blockchain_crypto-01-512.png';
const ethereumIcon = 'https://cdn2.iconfinder.com/data/icons/cryptocurrency-5/100/cryptocurrency_blockchain_crypto-02-512.png';
const xrpIcon = 'https://pbs.twimg.com/media/DfkiZGiW0AADnt9.png';

export { homeIcon, arrowLeft, bitcoinIcon, ethereumIcon, xrpIcon };
