/** Short names * */
const BTC = 'BTC';
const ETH = 'ETH';
const XRP = 'XRP';

/** Full names * */
const Bitcoin = 'Bitcoin';
const Ethereum = 'Ethereum';

export {BTC, ETH, XRP, Bitcoin, Ethereum};
