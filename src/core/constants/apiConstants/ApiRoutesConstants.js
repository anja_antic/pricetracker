import API_KEY from './ApiConfig';

/** Coins names, prices * */
const COINS_INFO = `https://api.nomics.com/v1/currencies/sparkline${API_KEY}`;

/** Start time for api route * */
const START_TIME_PREFIX = '&start=';
const START_HOURS_MINUTES_SECONDS = 'T00%3A00%3A00Z';

export {
    COINS_INFO,
    START_TIME_PREFIX,
    START_HOURS_MINUTES_SECONDS,
};
