const GET_COINS_INFO = 'get_coins_info';
const GET_COIN_DETAILS = 'get_coin_details';
const CLEAR_COIN_DETAILS = 'clear_coin_details';

export {GET_COINS_INFO, GET_COIN_DETAILS, CLEAR_COIN_DETAILS};
