const MontserratRegular = require('../../../../assets/fonts/Montserrat-Regular.ttf');
const MontserratBlack = require('../../../../assets/fonts/Montserrat-Black.ttf');
const MontserratLight = require('../../../../assets/fonts/Montserrat-Light.ttf');
const MontserratExtraLight = require('../../../../assets/fonts/Montserrat-ExtraLight.ttf');
const MontserratThin = require('../../../../assets/fonts/Montserrat-Thin.ttf');
const MontserratBold = require('../../../../assets/fonts/Montserrat-Bold.ttf');
const MontserratSemiBold = require('../../../../assets/fonts/Montserrat-SemiBold.ttf');
const MontserratMedium = require('../../../../assets/fonts/Montserrat-Medium.ttf');

/** Font Family **/
const montserratRegular = 'MontserratRegular';
const montserratBlack = 'MontserratBlack';
const montserratLight = 'MontserratLight';
const montserratExtraLight = 'MontserratExtraLight';
const montserratThin = 'MontserratThin';
const montserratBold = 'MontserratBold';
const montserratSemiBold = 'MontserratSemiBold';
const montserratMedium = 'MontserratMedium';

export {
  MontserratRegular,
  MontserratBlack,
  MontserratLight,
  MontserratExtraLight,
  MontserratThin,
  MontserratBold,
  MontserratSemiBold,
  MontserratMedium,
  montserratRegular,
  montserratBlack,
  montserratLight,
  montserratExtraLight,
  montserratThin,
  montserratBold,
  montserratSemiBold,
  montserratMedium,
};
