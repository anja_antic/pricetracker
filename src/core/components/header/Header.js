// @flow

import React from 'react';
import {
    View, Text, Image, TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {iconRegularColor} from '../../constants/styleConstants/Colors';
import {headerIconsSize} from '../../constants/styleConstants/Sizes';
import {
    arrowLeft, bitcoinIcon, ethereumIcon, xrpIcon,
} from '../../constants/iconsConstants/IconsConstants';
import {
    BTC, ETH, XRP, Bitcoin, Ethereum,
} from '../../constants/coinsConstants/CoinNames';
import styles from './styles';

type Props = {
    navigation: Object,
    coinName: string,
}

const Header = (props: Props) => {
    const {coinName, navigation} = props;

    /** Helpers * */
    const setCoinIcon = (name) => {
        const coinIcon = name === BTC ? bitcoinIcon : name === ETH ? ethereumIcon : name === XRP ? xrpIcon : null;
        return coinIcon;
    };

    const setCoinName = (name) => {
        switch (name) {
        case BTC:
            return Bitcoin;
        case ETH:
            return Ethereum;
        case XRP:
            return XRP;
        default:
            return null;
        }
    };
    /** * */
    return (
        <View style={styles.headerView}>
            <TouchableOpacity
                onPress={() => navigation.goBack()}
                activeOpacity={0.7}
                style={styles.backIcon}
            >
                <Icon
                    name={arrowLeft}
                    size={headerIconsSize}
                    color={iconRegularColor}
                />
            </TouchableOpacity>
            <Image
                source={{uri: setCoinIcon(coinName)}}
                style={styles.coinSymbol}
                resizeMode="contain"
            />
            <Text style={styles.coinName}>{setCoinName(coinName)}</Text>
        </View>
    );
};

export default Header;
