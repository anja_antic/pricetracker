import {StyleSheet} from 'react-native';
import {getStatusBarHeight, ifIphoneX} from 'react-native-iphone-x-helper';

import {coinSymbolSize, heading3} from '../../constants/styleConstants/Sizes';
import {headerBorderColor, regularFontColor} from '../../constants/styleConstants/Colors';
import {commonHeaderStyle} from '../../constants/styleConstants/CommonStyles';
import {montserratSemiBold} from '../../constants/fontsContstants/Fonts';

const styles = StyleSheet.create({
    headerView: {
        ...commonHeaderStyle,
        ...ifIphoneX({
            marginTop: getStatusBarHeight(),
        }),
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: headerBorderColor,
    },
    backIcon: {
        position: 'absolute',
        left: 20,
        bottom: 15,
    },
    coinSymbol: {
        width: coinSymbolSize,
        height: coinSymbolSize,
        marginRight: 10,
    },
    coinName: {
        fontFamily: montserratSemiBold,
        fontSize: heading3,
        color: regularFontColor,
    },
});

export default styles;
