// @flow

import React from 'react';
import {Text} from 'react-native';
import {LinearGradient} from 'expo-linear-gradient';

import {
    gradientStartColor,
    gradientMiddleColor,
    gradientEndColor,
} from '../../../core/constants/styleConstants/Colors';
import styles from './styles';

const HomeHeader = () => (
    <LinearGradient
        colors={[gradientStartColor, gradientMiddleColor, gradientEndColor]}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}
        style={styles.headerView}
    >
        <Text style={styles.text}>
            Price Tracker
        </Text>
    </LinearGradient>

);

export default HomeHeader;
