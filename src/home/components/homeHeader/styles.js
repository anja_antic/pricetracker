import { StyleSheet } from 'react-native';
import { getStatusBarHeight, ifIphoneX } from 'react-native-iphone-x-helper';

import { heading2 } from '../../../core/constants/styleConstants/Sizes';
import { invertFontColor } from '../../../core/constants/styleConstants/Colors';
import { montserratBold } from '../../../core/constants/fontsContstants/Fonts';
import { commonHeaderStyle } from '../../../core/constants/styleConstants/CommonStyles';

const styles = StyleSheet.create({
  headerView: {
    marginTop: 40,
    ...commonHeaderStyle,
    ...ifIphoneX({
      marginTop: getStatusBarHeight() + 20,
    }),
    alignItems: 'center',
    justifyContent: 'flex-end',
    margin: 20,
    borderRadius: 15,
  },
  text: {
    fontSize: heading2,
    fontFamily: montserratBold,
    letterSpacing: 2,
    color: invertFontColor,
  },
});

export default styles;
