// @flow

import React from 'react';
import {
    View, Text, Image, TouchableOpacity,
} from 'react-native';

import styles from './styles';
import {coinDetailsContainer} from '../../../core/constants/routesConstants/RoutesConstants';
import type {Coin} from '../../../core/models/Coin';
import {formatCurrentPrice} from '../../../core/helperFunctions';
import {BTC, ETH, XRP, Bitcoin, Ethereum} from '../../../core/constants/coinsConstants/CoinNames';
import {bitcoinIcon, ethereumIcon, xrpIcon} from '../../../core/constants/iconsConstants/IconsConstants';

type Props = {
    navigation: Object,
    coin: Coin,
}

const CoinCard = (props: Props) => {
    const {navigation, coin} = props;
    const {currency, prices} = coin;

    /** Helpers * */
    const setRightCoinIcon = (curr) => {
        const coinIcon = curr === BTC ? bitcoinIcon : curr === ETH ? ethereumIcon : curr === XRP ? xrpIcon : null;
        return coinIcon;
    };

    const setRightCoinName = (curr) => {
        const coinName = curr === BTC ? Bitcoin : curr === ETH ? Ethereum : curr === XRP ? XRP : null;
        return coinName;
    };
    /** * */
    return (
        <TouchableOpacity
            onPress={() => navigation.navigate(coinDetailsContainer, {
                coinName: currency,
            })}
            style={styles.cardView}
        >
            <View style={styles.coinNameAndSymbolView}>
                <Image
                    source={{uri: setRightCoinIcon(currency)}}
                    style={styles.coinSymbol}
                    resizeMode="contain"
                />
                <View style={styles.coinNamesView}>
                    <Text style={styles.coinShortName}>{currency}</Text>
                    <Text style={styles.coinFullName}>
                        {setRightCoinName(currency)}
                    </Text>
                </View>
            </View>
            <View>
                <Text style={styles.price}>
                    {formatCurrentPrice(prices)}
                </Text>
            </View>
        </TouchableOpacity>
    );
};


export default CoinCard;
