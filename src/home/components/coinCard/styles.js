import {StyleSheet} from 'react-native';

import {screenHeight, heading4, heading5, coinSymbolSize} from '../../../core/constants/styleConstants/Sizes';
import {montserratSemiBold} from '../../../core/constants/fontsContstants/Fonts';
import {lightBorderColor, lightFontColor, regularFontColor} from '../../../core/constants/styleConstants/Colors';

const styles = StyleSheet.create({
    cardView: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: screenHeight / 10,
        paddingTop: 10,
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: lightBorderColor,
    },
    coinNameAndSymbolView: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    coinSymbol: {
        width: coinSymbolSize,
        height: coinSymbolSize,
        marginRight: 10,
    },
    coinNamesView: {
        flexDirection: 'column',
    },
    coinShortName: {
        fontFamily: montserratSemiBold,
        fontSize: heading4,
        color: regularFontColor,
    },
    coinFullName: {
        fontFamily: montserratSemiBold,
        fontSize: heading5,
        color: lightFontColor,
    },
    price: {
        fontFamily: montserratSemiBold,
        fontSize: heading4,
        color: regularFontColor,
    },
});

export default styles;
