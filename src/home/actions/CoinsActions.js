// @flow

import axios from 'axios';
import type {Dispatch as ReduxDispatch} from 'redux';

import {COINS_INFO} from '../../core/constants/apiConstants/ApiRoutesConstants';
import {
    GET_COINS_INFO,
    GET_COIN_DETAILS,
    CLEAR_COIN_DETAILS,
} from '../../core/constants/actionsConstants/ActionsConstants';

export const getCoins = (startTime: string) => (dispatch: ReduxDispatch) => {
    const COINS_INFO_URL = COINS_INFO.concat(startTime);
    return (axios.get(COINS_INFO_URL)
        .then((result) => {
            console.log(COINS_INFO_URL);
            console.log('basic coins info', result.data);
            dispatch({
                type: GET_COINS_INFO,
                payload: result.data,
            });
        })
        .catch((error) => {
            console.log('ERROR', error);
        })
    );
};

export const getCoinDetails = (coinName: string, startTime: string) => (dispatch: ReduxDispatch) => {
    const COIN_INFO_URL = COINS_INFO.concat(startTime);
    return (axios.get(COIN_INFO_URL)
        .then((result) => {
            dispatch({
                type: GET_COIN_DETAILS,
                payload: result.data,
                coinName,
            });
        })
        .catch((error) => {
            console.log('ERROR', error);
        })
    );
};

export const clearCoinDetails = () => {
    return {
        type: CLEAR_COIN_DETAILS,
    };
};
