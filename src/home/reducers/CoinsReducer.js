import {GET_COINS_INFO} from '../../core/constants/actionsConstants/ActionsConstants';
import {BTC, ETH, XRP} from '../../core/constants/coinsConstants/CoinNames';

const INITIAL_STATE = {
    coins: [],
};

export default (state = INITIAL_STATE, action) => {
    const newState = {
        coins: [...state.coins],
    };
    switch (action.type) {
    case GET_COINS_INFO: {
        const favoriteCoins = action.payload.filter(coin => coin.currency === BTC || coin.currency === ETH || coin.currency === XRP);
        newState.coins = [...favoriteCoins];
        console.log('newStateCoins', newState.coins);
        return newState;
    }
    default:
        return state;
    }
};
