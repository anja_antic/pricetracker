import {GET_COIN_DETAILS, CLEAR_COIN_DETAILS} from '../../core/constants/actionsConstants/ActionsConstants';
import initialCoinState from '../../core/models/Coin';

const INITIAL_STATE = {
    coinDetails: {...initialCoinState},
};

export default (state = INITIAL_STATE, action) => {
    const newState = {
        coinDetails: {...state.coinDetails},
    };
    switch (action.type) {
    case GET_COIN_DETAILS: {
        const activeCoin = action.payload.filter(coin => coin.currency === action.coinName)[0];
        newState.coinDetails = activeCoin;
        console.log('activeCoin', newState.coinDetails);
        return newState;
    }
    case CLEAR_COIN_DETAILS:
        newState.coinDetails = {...initialCoinState};
        return newState;
    default:
        return state;
    }
};
