import {StyleSheet} from 'react-native';

import {heading1, heading4, heading5, heading6} from '../../../core/constants/styleConstants/Sizes';
import {
    lightFontColor,
    regularFontColor,
} from '../../../core/constants/styleConstants/Colors';
import {
    montserratExtraLight,
    montserratRegular,
    montserratSemiBold,
} from '../../../core/constants/fontsContstants/Fonts';

const styles = StyleSheet.create({
    coinInfoContainer: {
        padding: 20,
        paddingTop: 30,
    },
    currentPrice: {
        fontFamily: montserratSemiBold,
        fontSize: heading1,
        color: regularFontColor,
    },
    lastWeekHeadingView: {
      marginTop: 20,
        marginBottom: 20,
    },
    lastWeekHeading: {
        fontFamily: montserratRegular,
        fontSize: heading4,
        color: lightFontColor,
    },
    graphView: {
        flexDirection: 'row',
        height: 300,
    },
    yAxisInset: {
        top: 20,
        bottom: 20,
    },
    yAxisSvg: {
        fontFamily: montserratExtraLight,
        fontSize: heading6,
    },
    lineChartStyle: {
        flex: 1,
        marginLeft: 20,
    },
    lineChartInset: {
        top: 20,
        bottom: 20,
    },
});

export default styles;
