// @flow

import React from 'react';
import {
    View,
    Text,
    ScrollView,
    RefreshControl,
    ActivityIndicator,
} from 'react-native';
import {
    LineChart, YAxis, Grid,
} from 'react-native-svg-charts';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import type {Dispatch as ReduxDispatch} from 'redux';

import styles from './styles';
import {lightFontColor, lineChartColor, graphGridColor} from '../../../core/constants/styleConstants/Colors';
import Header from '../../../core/components/header/Header';
import {convertArrayOfStringsToArrayOfNumbers, formatCurrentPrice, formatDate} from '../../../core/helperFunctions';
import {getCoinDetails, clearCoinDetails} from '../../actions/CoinsActions';
import {START_HOURS_MINUTES_SECONDS, START_TIME_PREFIX} from '../../../core/constants/apiConstants/ApiRoutesConstants';
import type {Coin} from '../../../core/models/Coin';

type Props = {
    navigation: Object,
    coinDetails: Coin,
    getCoinDetails: (string, string) => Promise<void>,
    clearCoinDetails: () => Object,
}

type State = {
    refreshing: boolean,
    loading: boolean,
}

class CoinDetailsContainer extends React.Component<Props, State> {
    /** Global variables * */
    startTimeRoute: string;
    coinName: string;
    refreshCoinIntervalId: Number;

    constructor(props) {
        super(props);
        this.coinName = props.navigation.getParam('coinName');
        this.getCoinDetails = props.getCoinDetails;
        this.clearCoinDetails = props.clearCoinDetails;
    }

    state = {
        loading: true,
        refreshing: false,
    };

    componentDidMount() {
        this.getDateForSevenDaysAgo();
        console.log('route', this.startTimeRoute);
        this.getCoinDetails(this.coinName, this.startTimeRoute)
            .then(() => this.setState({loading: false}))
            .catch((error) => {
                this.setState({loading: false});
                console.log(error);
            });
        this.refreshCoinIntervalId = setInterval(() => this.onRefresh(false), 30000);
    }

    componentWillUnmount() {
        clearInterval(this.refreshCoinIntervalId);
        this.clearCoinDetails();
    }

    /** Helpers * */
    getDateForSevenDaysAgo() {
        const currentDate = new Date();
        // currentDate.getTime() -> gives us miliseconds from 1970 till today
        // 604800000 -> miliseconds for 7 days
        const date7DaysAgo = new Date(currentDate.getTime() - 604800000);
        const year7DaysAgo = date7DaysAgo.getFullYear();
        const month7DaysAgo = formatDate(date7DaysAgo.getMonth() + 1);
        const day7DaysAgo = formatDate(date7DaysAgo.getDate());
        this.startTimeRoute = `${START_TIME_PREFIX}${year7DaysAgo}-${month7DaysAgo}-${day7DaysAgo}${START_HOURS_MINUTES_SECONDS}`;
    }

    onRefresh(isVisible) {
        this.setState({refreshing: isVisible});
        this.getCoinDetails(this.coinName, this.startTimeRoute)
            .then(() => this.setState({refreshing: false}))
            .catch(error => console.log('errorRefreshing:', error));
    }

    /** Render * */
    render() {
        const {navigation, coinDetails} = this.props;
        const {refreshing, loading} = this.state;
        return (
            <View>
                {loading ? (
                    <View>
                        <Header navigation={navigation} coinName={this.coinName} />
                        <View style={{marginTop: 100}}>
                            <ActivityIndicator size="large" />
                        </View>
                    </View>
                )
                    : (
                        <View>
                            <Header navigation={navigation} coinName={this.coinName} />

                            <ScrollView
                                contentContainerStyle={styles.coinInfoContainer}
                                refreshControl={(
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={() => this.onRefresh(true)}
                                    />
                                )}
                            >
                                <View>
                                    <Text style={styles.currentPrice}>
                                        {formatCurrentPrice(coinDetails.prices)}
                                    </Text>
                                </View>

                                {/** Last week heading * */}
                                <View style={styles.lastWeekHeadingView}>
                                    <Text style={styles.lastWeekHeading}>LAST 7 DAYS</Text>
                                </View>

                                {/** Graph * */}
                                <View style={styles.graphView}>
                                    <YAxis
                                        data={convertArrayOfStringsToArrayOfNumbers(coinDetails.prices)}
                                        contentInset={styles.yAxisInset}
                                        svg={{
                                            ...styles.yAxisSvg,
                                            stroke: lightFontColor,
                                        }}
                                        numberOfTicks={7}
                                        formatLabel={value => value}
                                    />
                                    <LineChart
                                        style={styles.lineChartStyle}
                                        data={convertArrayOfStringsToArrayOfNumbers(coinDetails.prices)}
                                        contentInset={styles.lineChartInset}
                                        svg={{
                                            stroke: lineChartColor,
                                            strokeWidth: 2,
                                        }}
                                    >
                                        <Grid svg={{stroke: graphGridColor}} />
                                    </LineChart>
                                </View>

                            </ScrollView>
                        </View>
                    )
                }

            </View>
        );
    }
}

const mapStateToProps = state => ({
    coinDetails: state.coinDetails.coinDetails,
});

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
    bindActionCreators({
        getCoinDetails,
        clearCoinDetails,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(CoinDetailsContainer);
