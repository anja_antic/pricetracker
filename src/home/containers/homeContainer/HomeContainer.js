// @flow

import React from 'react';
import {
    View, Text, FlatList, ScrollView, RefreshControl,
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import type {Dispatch as ReduxDispatch} from 'redux';

import styles from './styles';
import {getCoins} from '../../actions/CoinsActions';
import HomeHeader from '../../components/homeHeader/HomeHeader';
import CoinCard from '../../components/coinCard/CoinCard';
import {formatDate} from '../../../core/helperFunctions';
import {START_TIME_PREFIX, START_HOURS_MINUTES_SECONDS} from '../../../core/constants/apiConstants/ApiRoutesConstants';
import type {Coin} from '../../../core/models/Coin';

type Props = {
    navigation: Object,
    coins: Array<Coin>,
    getCoins: (string) => Promise<void>,
}

type State = {
    refreshing: boolean,
}

class HomeContainer extends React.Component<Props, State> {
    /** Global variables * */
    refreshCoinIntervalId: Number;
    startTimeRoute: string;

    constructor(props) {
        super(props);
        this.getCoins = props.getCoins;
    }
    state = {
        refreshing: false,
    };

    componentDidMount() {
        this.refreshCoinIntervalId = setInterval(() => this.onRefresh(false), 30000);
        this.getDateForSevenDaysAgo();
        console.log('route', this.startTimeRoute);
        this.getCoins(this.startTimeRoute);
    }

    componentWillUnmount() {
        clearInterval(this.refreshCoinIntervalId);
    }

    /** Helpers * */
    getDateForSevenDaysAgo() {
        const currentDate = new Date();
        // currentDate.getTime() -> gives us miliseconds from 1970 till today
        // 604800000 -> miliseconds for 7 days
        const date7DaysAgo = new Date(currentDate.getTime() - 604800000);
        const year7DaysAgo = date7DaysAgo.getFullYear();
        const month7DaysAgo = formatDate(date7DaysAgo.getMonth() + 1);
        const day7DaysAgo = formatDate(date7DaysAgo.getDate());
        this.startTimeRoute = `${START_TIME_PREFIX}${year7DaysAgo}-${month7DaysAgo}-${day7DaysAgo}${START_HOURS_MINUTES_SECONDS}`;
    }

    onRefresh(isVisible) {
        this.setState({refreshing: isVisible});
        this.getCoins(this.startTimeRoute)
            .then(() => this.setState({refreshing: false}))
            .catch(error => console.log('errorRefreshing:', error));
    }

    /** Render * */
    render() {
        const {navigation, coins} = this.props;
        const {refreshing} = this.state;
        return (
            <View>
                <HomeHeader />
                <ScrollView
                    contentContainerStyle={styles.scrollView}
                    showsVerticalScrollIndicator={false}
                    refreshControl={(
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={() => this.onRefresh(true)}
                        />
                    )}
                >
                    <View style={styles.headingsView}>
                        <Text style={styles.heading}>COIN</Text>
                        <Text style={styles.heading}>PRICE</Text>
                    </View>
                    <FlatList
                        data={coins}
                        keyExtractor={item => item.currency}
                        renderItem={({item}) => <CoinCard coin={item} navigation={navigation} />}
                    />
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => ({
    coins: state.coins.coins,
});

const mapDispatchToProps = (dispatch: ReduxDispatch): Function => (
    bindActionCreators({
        getCoins,
    }, dispatch)
);

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);
