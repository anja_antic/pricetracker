import { StyleSheet } from 'react-native';

import { heading5 } from '../../../core/constants/styleConstants/Sizes';
import { montserratSemiBold } from '../../../core/constants/fontsContstants/Fonts';
import { lightFontColor, lightBorderColor } from '../../../core/constants/styleConstants/Colors';

const styles = StyleSheet.create({
  scrollView: {
    height: '100%',
    paddingLeft: 20,
    paddingRight: 20,
  },
  headingsView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: lightBorderColor,
    paddingBottom: 15,
    marginTop: 30,
  },
  heading: {
    fontFamily: montserratSemiBold,
    color: lightFontColor,
    fontSize: heading5,
  },
});

export default styles;
