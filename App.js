// @flow

import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import * as Font from 'expo-font';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import ReduxThunk from 'redux-thunk';

import reducers from './src/core/reducers';
import {
    MontserratRegular,
    MontserratBlack,
    MontserratLight,
    MontserratExtraLight,
    MontserratThin,
    MontserratBold,
    MontserratSemiBold,
    MontserratMedium,
} from './src/core/constants/fontsContstants/Fonts';
import AppContainer from './src/navigation/tabNavigator/TabNavigator';

type State = {
    fontLoaded: boolean,
}

export default class App extends React.Component<null, State> {
    state = {
        fontLoaded: false,
    };

    async componentDidMount() {
        await Font.loadAsync({
            MontserratRegular,
            MontserratBlack,
            MontserratLight,
            MontserratExtraLight,
            MontserratThin,
            MontserratBold,
            MontserratSemiBold,
            MontserratMedium,
        });

        this.setState({fontLoaded: true});
    }

    render() {
        const {state} = this;
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <View style={{flex: 1}}>
                {state.fontLoaded ? (
                    <Provider store={store}>
                        <AppContainer />
                    </Provider>
                ) : (
                    <View style={{marginTop: 300}}>
                        <ActivityIndicator size="large" />
                    </View>
                )
                }
            </View>
        );
    }
}
